﻿using System.Linq;
using System;
using OmiLAXR.Interaction;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.InputSystem.XR;
using UnityEngine.XR.Interaction.Toolkit;
namespace OmiLAXR.Adapters.UnityXR
{
    public class XRPlayer : IPlayer
    {
        private XROrigin _xrOrigin;
        private CharacterControllerDriver _characterControllerDriver;
        private readonly Transform _cameraOffset;
        private readonly Transform _cameraTransform;

        private readonly GameObject _leftHand;
        private readonly GameObject _rightHand;
        
        private static readonly DebugLog _debug = new DebugLog("UnityXR Adapter > XRPlayer");
        public static DebugLog DebugLog => _debug;
        
        public XRPlayer(XROrigin xrOrigin, CharacterControllerDriver characterControllerDriver)
        {
            var camera = xrOrigin.transform.GetComponentInChildren(typeof(Camera));
            var cameraParent = camera.transform.parent;

            if (cameraParent != xrOrigin.transform)
                _cameraOffset = cameraParent;


            _leftHand = FindHand("left");
            _rightHand = FindHand("right");
            
            _cameraTransform = camera.transform;
        }

        private static GameObject FindHand(string identifier)
        {
            var trackedPoseDrivers = UnityEngine.Object.FindObjectsOfType<TrackedPoseDriver>();
            if (trackedPoseDrivers == null)
            {
                DebugLog.Error("Cannot find 'TrackedPoseDriver', which is important to identify hands.");
                return null;
            }
            var trackedPoseDriver = trackedPoseDrivers.First(tpd => tpd.name.ToLower().Contains(identifier.ToLower()));
            return trackedPoseDriver.gameObject;
        }

        public Transform GetTransform() => _cameraTransform;
        public Vector3 WorldPosition => GetTransform().position;
        public Vector3 WorldRotation => GetCamera().transform.eulerAngles;

        public Transform GetHead() => _cameraTransform;

        public Transform GetBody() => _cameraOffset;

        public ILaserPointer GetLeftPointer()
        {
            throw new NotImplementedException("Coming soon");
        }

        public ILaserPointer GetRightPointer()
        {
            throw new NotImplementedException("Coming soon");
        }

        public GameObject GetLeftHand() => _leftHand;
        public GameObject GetRightHand() => _rightHand;

        public Camera GetCamera() => Camera.main;
        public bool IsVR => true;

        private static string[] _actions = { "Position", "Rotation", "Select", "Activate", "UI Press", "Grab" };
        [Obsolete("Has to be implemented into dynamic list.")]
        public string[] GetControllerActions() => _actions;

        public event ActionChangedHandler OnActionChanged;
    }
}