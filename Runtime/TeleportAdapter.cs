﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OmiLAXR.Adapters.UnityXR
{
    [AddComponentMenu("GameObject / OmiLAXR / (OmiLAXR) Teleport Adapter for UnityXR")]
    public class TeleportAdapter : MonoBehaviour
    {
        public LocomotionSystem locomotionSystem;

        private TeleportationArea[] _teleportationAreas;
        private TeleportationAnchor[] _teleportAnchors;
        
        protected void Start()
        {
            if (!locomotionSystem)
                locomotionSystem = FindObjectOfType<LocomotionSystem>();

            _teleportationAreas = FindObjectsOfType<TeleportationArea>();
            _teleportAnchors = FindObjectsOfType<TeleportationAnchor>();
            
            BindEvents(_teleportationAreas);
            BindEvents(_teleportAnchors);
        }

        private void BindEvents(IEnumerable<BaseTeleportationInteractable> tas)
        {
            foreach (var ta in tas)
            {
                ta.teleporting.AddListener(OnTeleporting);
                ta.hoverEntered.AddListener(OnHoverEntered);
                ta.hoverExited.AddListener(OnHoverExited);
            }
        }

        private void OnHoverExited(HoverExitEventArgs arg0)
        {
            print("exited");
        }

        private void OnHoverEntered(HoverEnterEventArgs arg0)
        {
            print("entered");
        }

        private void OnTeleporting(TeleportingEventArgs arg0)
        {
            print("teleporting");
        }
    }
}