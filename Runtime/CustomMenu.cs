﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace OmiLAXR.Adapters.UnityXR
{
    internal class CustomMenu
    {
        [MenuItem("GameObject / OmiLAXR / Adapters / UnityXR Adapter")]
        private static void AddUnityXRAdapter()
        {
            var selectedGameObject = Selection.activeGameObject;
            var prefab = Resources.Load<GameObject>("Prefabs/UnityXR Adapter");
            if (selectedGameObject)
                PrefabUtility.InstantiatePrefab(prefab, selectedGameObject.transform);
            else
                PrefabUtility.InstantiatePrefab(prefab);
        }
    }
}
#endif