﻿using OmiLAXR.Interaction;
using UnityEngine;

namespace OmiLAXR.Adapters.UnityXR
{
    public class NonXRPlayer : IPlayer
    {
        private readonly NonXRController _nonXRController;
        public NonXRPlayer(NonXRController nonXRController)
        {
            _nonXRController = nonXRController;
        }

        public Transform GetTransform() => _nonXRController.playerCamera.transform;

        public Transform GetHead() => _nonXRController.playerCamera.transform;

        public Transform GetBody() => _nonXRController.playerCamera.transform;

        public ILaserPointer GetLeftPointer()
        {
            throw new System.NotImplementedException("Coming Soon");        
        }

        public ILaserPointer GetRightPointer()
        {
            throw new System.NotImplementedException("Coming Soon");
        }

        public GameObject GetLeftHand() => _nonXRController ? _nonXRController.interactionPoint : null;

        public GameObject GetRightHand() => _nonXRController ? _nonXRController.interactionPoint : null;

        public Camera GetCamera() => _nonXRController.playerCamera;
        public Vector3 WorldPosition => GetTransform().position;
        public Vector3 WorldRotation => GetCamera().transform.eulerAngles;

        public bool IsVR => false;
        public string[] GetControllerActions() => new[] {"Keyboard", "Mouse", "GamePad"};
        public event ActionChangedHandler OnActionChanged;
    }
}