﻿using System;
using OmiLAXR.Interaction;
using OmiLAXR.xAPI.Tracking;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OmiLAXR.Adapters.UnityXR
{
    [RequireComponent(typeof(XRBaseInteractable)), RequireComponent(typeof(Trackable))]
    public class InteractableAdapter : MonoBehaviour, IInteractable
    {
        private XRBaseInteractable _interactable;
        private Trackable _trackable;

        private void Awake()
        {
            _interactable = GetComponent<XRBaseInteractable>();
            _trackable = GetComponent<Trackable>();
        }

        private static readonly DebugLog _debug = new DebugLog("UnityXR Adapter > Interactable Adapter");
        public static DebugLog DebugLog => _debug;
        
        private void OnEnable() => SetupListeners();

        private void OnDisable() => RemoveListeners();

        private void OnDestroy() => RemoveListeners();

        #region IInteractable Functionality

        public void SetHighlightOnHover(bool highlightValue)
        {
            throw new NotImplementedException("Coming Soon");
        }

        public bool IsHovering() => _interactable.isHovered;

        public bool IsSelected() => _interactable.isSelected;

        #endregion

        #region Listeners Functionality

        private void SetupListeners()
        {
            _interactable.firstHoverEntered.AddListener(OnFirstHoverEntered);
            _interactable.hoverEntered.AddListener(OnHoverEntered);
            _interactable.hoverExited.AddListener(OnHoverExited);
            _interactable.lastHoverExited.AddListener(OnLastHoverExited);

            _interactable.firstSelectEntered.AddListener(OnFirstSelectEntered);
            _interactable.selectEntered.AddListener(OnSelectEntered);
            _interactable.selectExited.AddListener(OnSelectExited);
            _interactable.lastSelectExited.AddListener(OnLastSelectExited);

            _interactable.activated.AddListener(OnActivated);
            _interactable.deactivated.AddListener(OnDeactivated);
        }

        private void RemoveListeners()
        {
            _interactable.firstHoverEntered.RemoveListener(OnFirstHoverEntered);
            _interactable.hoverEntered.RemoveListener(OnHoverEntered);
            _interactable.hoverExited.RemoveListener(OnHoverExited);
            _interactable.lastHoverExited.RemoveListener(OnLastHoverExited);

            _interactable.firstSelectEntered.RemoveListener(OnFirstSelectEntered);
            _interactable.selectEntered.RemoveListener(OnSelectEntered);
            _interactable.selectExited.AddListener(OnSelectExited);
            _interactable.lastSelectExited.RemoveListener(OnLastSelectExited);

            _interactable.activated.RemoveListener(OnActivated);
            _interactable.deactivated.RemoveListener(OnDeactivated);
        }

        #endregion

        #region Hover Events Functionality

        private void OnFirstHoverEntered(HoverEnterEventArgs arg0)
        {
            DebugLog.Print($"OnFirstHoverEntered: {gameObject.name}");
        }

        private void OnHoverEntered(HoverEnterEventArgs arg0)
        {
            DebugLog.Print($"OnHoverEntered: {gameObject.name}");
        }

        private void OnHoverExited(HoverExitEventArgs arg0)
        {
            DebugLog.Print($"OnHoverExited: {gameObject.name}");
        }

        private void OnLastHoverExited(HoverExitEventArgs arg0)
        {
            DebugLog.Print($"OnLastHoverExited: {gameObject.name}");
        }

        #endregion

        #region Select Event Functionality

        private void OnFirstSelectEntered(SelectEnterEventArgs arg0)
        {
            DebugLog.Print($"OnFirstSelectEntered: {gameObject.name}");
        }

        private void OnSelectEntered(SelectEnterEventArgs arg0)
        {
            DebugLog.Print($"OnSelectEntered: {gameObject.name}");
        }

        private void OnSelectExited(SelectExitEventArgs arg0)
        {
            DebugLog.Print($"OnSelectExited: {gameObject.name}");
        }

        private void OnLastSelectExited(SelectExitEventArgs arg0)
        {
            DebugLog.Print($"OnLastSelectExited: {gameObject.name}");
        }

        #endregion

        #region Activate Event Functionality

        private void OnActivated(ActivateEventArgs arg0)
        {
            DebugLog.Print($"OnActivated: {gameObject.name}");
        }

        private void OnDeactivated(DeactivateEventArgs arg0)
        {
            DebugLog.Print($"OnDeactivated: {gameObject.name}");
        }

        #endregion
    }
}