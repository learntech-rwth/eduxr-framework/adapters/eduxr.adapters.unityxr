﻿using UnityEngine;

using Unity.XR.CoreUtils;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Inputs;

namespace OmiLAXR.Adapters.UnityXR
{
    [RequireComponent(typeof(TeleportAdapter))]
    [DisallowMultipleComponent]
    [AddComponentMenu("OmiLAXR / Adapters / Unity XR Adapter (needed for OmiLAXR)")]
    [DefaultExecutionOrder(-5)]
    public class UnityXRAdapter : XR_Framework_Adapter
    {
        private static UnityXRAdapter _instance;
        /// <summary>
        /// Singleton instance of the UnityXR_Adapter. Only one can exist at a time.
        /// </summary>
        public static UnityXRAdapter Instance
        {
            get
            {
                if ( _instance == null )
                {
                    _instance = FindObjectOfType<UnityXRAdapter>();

                    if (_instance == null)
                    {
                        DebugLog.Error("Cannot find UnityXR Adapter component.");
                    }
                }
                return _instance;
            }
        }
        private static readonly DebugLog _debug = new DebugLog("UnityXR Adapter");
        public static DebugLog DebugLog => _debug;
        public NonXRController nonXRController;
        
        public XROrigin xrOrigin;
        public InputActionManager inputActionManager;

        protected void Start()
        {
            LoadPlayer();
        }

        public override MonoBehaviour GetInstance() => Instance;

        //public static bool CanVR => XRSettings.enabled;
        public static bool CanVR => UnityEngine.XR.Management.XRGeneralSettings.Instance.Manager.activeLoader is not null;

        private CharacterControllerDriver GetCharacter()
        {
            var characterController = FindObjectOfType<CharacterControllerDriver>();
            if (characterController)
                playerReference = characterController.gameObject;
            return characterController;
        }
        
        protected override GameObject LoadPlayer()
        {
            if (!Learner)
                DebugLog.Error("Please add a 'Learner' GameObject to the scene.");
            
            var playerController = GetCharacter();

            if (CanVR)
            {
                if (!xrOrigin)
                    xrOrigin = FindObjectOfType<XROrigin>();
                
                Learner.AssignPlayer(new XRPlayer(xrOrigin, playerController));
            }
            else
            {
                if (!nonXRController)
                    nonXRController = FindObjectOfType<NonXRController>();
                
                Learner.AssignPlayer(new NonXRPlayer(nonXRController));
            }

            if (playerController != null)
                return playerController.gameObject;
            
            return nonXRController != null ? nonXRController.gameObject : xrOrigin.gameObject;
        }

        protected override bool IsVR() => XRSettings.enabled;

        public override bool IsLoading() => false;
    }
}