using System.Collections.Generic;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem.XR;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation;
using UnityEngine.XR.Management;

namespace OmiLAXR.Adapters.UnityXR
{
    [RequireComponent(typeof(CharacterController))]
    public class NonXRController : MonoBehaviour
    {
        public Camera playerCamera;

        [Tooltip("Always join via desktop controls")]
        public bool ignoreHeadsetConnection = false;

        [Tooltip("The speed to walk around using W A S D.")]
        public float walkSpeed = 5;

        [Tooltip("The speed to look around using the mouse.")]
        public float lookSpeed = 2;

        [Tooltip("The gameobject used as controller")]
        public GameObject interactionPoint;

        [Tooltip("The speed to set the distance of the interaction point using the scroll wheel")]
        public float scrollSpeed = 0.05f;

        [Tooltip("The maximal distance of the interaction point")]
        public float maxInteractionPointDistance = 2f;

        public UnityEvent OnUse = new UnityEvent();

        public UnityEvent OnNotUse = new UnityEvent();

        public bool IsHmdAvailable { get; private set; }

        private CharacterController characterController;
        private float distanceMultiplier = 1f;
        private int ignoreRaycastLayer = 2;


        private XRBaseInteractor interactor;
        private float rotX = 0f;
        private float maxX = 80f;
        private float minX = -85f;
        private int originalLayer;
        private Dictionary<GameObject, int> savedlayers = new Dictionary<GameObject, int>();

        private Vector3 originalCameraPosition;
        private Quaternion originalCameraRotation;
        
        private static readonly DebugLog _debug = new DebugLog("UnityXR Adapter > NonXRController");
        public static DebugLog DebugLog => _debug;

        private void Awake()
        {
            if (!enabled)
                return;

            playerCamera ??= Camera.main;
            if (playerCamera is null)
                throw new MissingReferenceException();
            if (interactionPoint is null)
                throw new MissingReferenceException();

            var cameraTransform = playerCamera.transform;
            originalCameraPosition = cameraTransform.localPosition;
            originalCameraRotation = cameraTransform.localRotation;

            SetUpNonXRController();
        }


        // Update is called once per frame
        private void Update()
        {
            if (interactionPoint is null)
                return;
            MovePlayer();

            if (!Input.GetMouseButton(1))
                LookWithPlayer();
            else
                SetInteractionPointRotation();

            SetInteractionPointDistance();
            SetInteractionPointHitPosition();
        }

        private void OnDestroy()
        {
            if (!enabled)
                return;
            if (interactor)
            {
                interactor.selectEntered.RemoveListener(EnableIgnoreRaycast);
                interactor.selectExited.RemoveListener(DisableIgnoreRaycast);
            }
        }

        private void OnEnable() => SetUpNonXRController();

        private void OnDisable()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            PrepareMode(desktop: false);
            
            if (interactor)
            {
                interactor.selectEntered.RemoveListener(EnableIgnoreRaycast);
                interactor.selectExited.RemoveListener(DisableIgnoreRaycast);
            }
        }

        private bool HeadsetAvailable()
        {
            if (XRGeneralSettings.Instance.Manager.activeLoader is null)
            {
                DebugLog.Print("No headset detected!");
                IsHmdAvailable = false;
                return false;
            }

            IsHmdAvailable = true;
            DebugLog.Print("Headset detected!");
            return true;
        }

        private void MovePlayer()
        {
            var speedW = Input.GetKey(KeyCode.W) ? walkSpeed : 0;
            var speedA = Input.GetKey(KeyCode.A) ? -walkSpeed : 0;
            var speedS = Input.GetKey(KeyCode.S) ? -walkSpeed : 0;
            var speedD = Input.GetKey(KeyCode.D) ? walkSpeed : 0;

            var tr = transform;
            var moveDirectionForwards = (speedW + speedS) * tr.forward;
            var moveDirectionSidewards = (speedA + speedD) * tr.right;
            var moveDirection = moveDirectionForwards + moveDirectionSidewards;

            if (!characterController.isGrounded)
                moveDirection.y -= 10000f * Time.deltaTime;

            characterController.Move(moveDirection * Time.deltaTime);
        }

        private void LookWithPlayer()
        {
            rotX = Mathf.Clamp(rotX + Input.GetAxis("Mouse Y") * lookSpeed, minX, maxX);
            playerCamera.transform.localRotation = Quaternion.Euler(-rotX, 0, 0);

            transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * lookSpeed, 0);
        }

        private void SetInteractionPointDistance()
        {
            var scroll = Input.mouseScrollDelta.y * scrollSpeed;
            if (scroll == 0)
                return;
            distanceMultiplier = Mathf.Clamp(distanceMultiplier + scroll, 0.1f, 1);
        }

        private void SetInteractionPointHitPosition()
        {
            var camTr = playerCamera.transform;
            var ray = new Ray(camTr.position, camTr.forward);
            if (!Physics.Raycast(ray, out var hit))
                return;
            var cameraPosition = playerCamera.transform.position;
            var interactionPointDistance = maxInteractionPointDistance * distanceMultiplier;
            interactionPoint.transform.position = hit.distance < interactionPointDistance
                ? hit.point
                : cameraPosition + interactionPointDistance * (hit.point - cameraPosition).normalized;
        }

        private void SetInteractionPointRotation()
        {
            interactionPoint.transform.Rotate(playerCamera.transform.forward, -Input.GetAxis("Mouse X") * lookSpeed,
                Space.World);
            interactionPoint.transform.Rotate(transform.right, Input.GetAxis("Mouse Y") * lookSpeed, Space.World);
        }

        private void EnableIgnoreRaycast(SelectEnterEventArgs args) =>
            SaveAndReplaceLayer(args.interactableObject.transform.gameObject, ignoreRaycastLayer);

        private void DisableIgnoreRaycast(SelectExitEventArgs args) => RestoreLayers();

        private void SaveAndReplaceLayer(GameObject parent, int newLayer)
        {
            foreach (var childTransform in parent.GetComponentsInChildren<Transform>())
            {
                var childGameObject = childTransform.gameObject;
                if (savedlayers.ContainsKey(childGameObject))
                    continue;
                savedlayers.Add(childGameObject, childGameObject.layer);
                childGameObject.layer = newLayer;
            }
        }

        private void RestoreLayers()
        {
            foreach (var (savedGameObject, layer) in savedlayers)
                savedGameObject.layer = layer;
            savedlayers.Clear();
        }

        protected virtual void PrepareMode(bool desktop = true)
        {
            playerCamera.GetComponent<TrackedPoseDriver>().enabled = !desktop;
            
            foreach (var simulator in FindObjectsOfType<XRDeviceSimulator>(true))
                simulator.gameObject.SetActive(desktop);

            foreach (var tpd in FindObjectsOfType<TrackedPoseDriver>(true))
            {
                var tpdGameObject = tpd.gameObject;
                if (tpdGameObject != interactionPoint && tpdGameObject != playerCamera.gameObject)
                    tpdGameObject.SetActive(!desktop);
            }
                
            interactionPoint.SetActive(desktop);
            
            if (desktop)
                OnUse.Invoke();
            else
                OnNotUse.Invoke();
        }

        private void SetUpNonXRController()
        {
            if (!ignoreHeadsetConnection && HeadsetAvailable())
            {
                DebugLog.Print("Disabling non-xr-controller!");
                enabled = false;
                return;
            }

            var cameraTransform = playerCamera.transform;
            cameraTransform.localPosition = originalCameraPosition;
            cameraTransform.localRotation = originalCameraRotation;

            DebugLog.Print("Using non-xr-controller!");
            PrepareMode(desktop: true);

            characterController = GetComponent<CharacterController>();

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            
            // Ensure ignore raycast
            interactionPoint.SetLayerRecursively(ignoreRaycastLayer);

            interactor = interactionPoint.GetComponentInChildren<XRBaseInteractor>();
            if (interactor)
            {
                interactor.selectEntered.AddListener(EnableIgnoreRaycast);
                interactor.selectExited.AddListener(DisableIgnoreRaycast);
            }
        }
    }
}